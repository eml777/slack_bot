# Weekday : Friday == 4

import os
import time

from timeloop import Timeloop
from datetime import timedelta, datetime
import pytz
from decouple import config

import slack
from slack import RTMClient

friday_counter = 0
tl = Timeloop()
slack_token = config('SLACK_BOT_TOKEN')
client = slack.WebClient(slack_token)


def send_reminder_message(title):
    client.chat_postMessage(
        channel='#general',
        link_names=1,
        parse='full',
        text='{} @here :loudspeaker: Recuerda registrar tus horas de trabajo https://docs.google.com/forms/d/e/1FAIpQLSdt_gm5BRGdIQXMsFsWP7vn_FJd6BMLKQvEreflQ7AYzhnUog/viewform'.format(
            title)
    )


@RTMClient.run_on(event="hello")
def connected(**kwargs):
    print('BOT Conectado')


@RTMClient.run_on(event="message")
def say_hello(**payload):
    data = payload['data']
    web_client = payload['web_client']
    try:
        channel_id = data.get('channel')
        thread_ts = data.get('ts')
        user = data.get('user')
        master_message = data.get('text')
        master_message.lower()
        if master_message.lower().startswith('$help'):

            web_client.chat_postMessage(
                channel=channel_id,
                blocks=[
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": "*Hola*:robot_face: sitios que pueden ser de *ayuda*"
                        }
                    },
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":newspaper: <https://accounts.google.com/ServiceLogin?service=wise&passive=1209600&continue=https://docs.google.com/document/u/1/d/1hb-LpWfd_DeIJoAq_Y-93xlUxbNTDbRwZj_THhKKw3Y/edit&followup=https://docs.google.com/document/u/1/d/1hb-LpWfd_DeIJoAq_Y-93xlUxbNTDbRwZj_THhKKw3Y/|*Newsletter*> \t\t\t\t\t\t\t\t\t   Para ponerse al día. \n :bookmark_tabs: <http://tinyurl.com/y4jqcqe4|*Facturas y Viáticos*> \t\t\t Herramienta para reportar. \n :airplane: <https://bit.ly/2LmfGO2|*Vuelos*> \t\t\t\t\t\t\t\t\t   Formulario de Solicitud. \n :card_index_dividers: <https://app.asana.com/0/946256802696834/overview|*ASANA*> \t\t\t\t   Seguimiento de proyectos y tareas. \n :gestalabs: <https://drive.google.com/drive/u/0/folders/17GMMqaJjbGS-LHmoz2YY_ry3KwaQt9fC|*Equipo*> \t\t\t\t\t\t\t\t\t\t\t  Conoce al crew :the_horns:. \n :frame_with_picture: <https://drive.google.com/drive/u/0/folders/1L42TguNSN9tCKL96L4QcYxvsCNBswpyX|*Logos*> \t\t\t\t\t\t\t\t\t\t\t    Logos de Gestalabs. \n :books: <https://drive.google.com/drive/u/0/folders/1eETiTC9l4gG1HSB58xyZ6Pc1FvvgoLTe|*Recursos*> \t Imágenes y recursos para Presentaciones. \n :page_facing_up: <https://drive.google.com/drive/u/0/folders/1F-Xd22pnzcFYQTYmr8-PpMKFv8xeuZr5|*Formatos*>   Documentos para Minutas, Checklists, etc."
                        },
                        "accessory": {
                            "type": "image",
                            "image_url": "https://lh3.googleusercontent.com/u/0/d/1niFA2ya7FZWMB-YfxUiTrOkdPUalyQAQ=w926-h625-iv1",
                            "alt_text": "Gestalabs LOGO"
                        }
                    }
                ]
            )

    except Exception as e:
        print('ERROR ERROR ERROR : {}'.format(e))


@tl.job(interval=timedelta(minutes=1))
def job_verify_date_every_hour():
    global friday_counter
    mx_now = datetime.now(pytz.timezone('America/Mexico_City'))
    print(mx_now)
    try:
        if mx_now.weekday() == 4:
            if mx_now.hour == 12:
                if friday_counter < 1:
                    friday_counter = 1
                    send_reminder_message('Primera Llamada')
            elif mx_now.hour == 16:
                if friday_counter < 2:
                    friday_counter = 2
                    send_reminder_message('Segunda Llamada')
            elif mx_now.hour == 18:
                friday_counter = 0
            else:
                pass
    except Exception as e:
        print('ERROR ERROR ERROR : {}'.format(e))


if __name__ == "__main__":
    tl.start(block=False)
    rtm_client = RTMClient(token=slack_token)
    rtm_client.start()
